resource "google_container_cluster" "gke-cluster" {
  name               = "gke-tf-demo-280516-gke-cluster"
  network            = "default"
  location           = "us-central1"
  initial_node_count = 1
}
