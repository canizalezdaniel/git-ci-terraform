terraform {
  backend "gcs" {
    bucket = "gke-tf-demo-280516-tfstate"
    credentials = "./creds/serviceaccount.json"
  }
}
