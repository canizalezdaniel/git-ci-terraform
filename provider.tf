provider "google" {
  credentials = file("./creds/serviceaccount.json")
  project     = "gke-tf-demo-280516"
  region      = "us-central1"
}
